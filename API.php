<?php

    /**
     * LyrisHQ API Library
     * @author Clark Tomlinson
     * @package LyrisAPI
     * @since Jun 19, 2012, 12:16:32 PM
     * @link http://www.clarkt.com
     * @copyright 2012
	 *
	 * @fork Cast-Control
	 *
     */

    namespace CastControl\Lyris;

    /**
     * Handles all LyrisHQ API interactions
     */
    class API
        {

        /**
         * Stores siteID for class functions
         * @var
         */
            private $siteid;

        /**
         * Stores global api password for all requests
         * @var null
         */
            private $apipassword;
			
        /**
         * Stores CURL connection
         * @var null
         */
            private $ch;


        /**
         * Gathers Site id and global api password for requests
         * @param      $siteid
         * @param null $apipassword
         */
            public function __construct($url, $siteid, $apipassword = null)
                {
					//set url
					$this->url = $url;
					
                    //set site id
                    $this->siteid = $siteid;

                    //set global api password
                    if (!is_null($apipassword)) {
                        $this->apipassword = $apipassword;
                    }
                }

        /**
         * Attribute / Demographic FUNCTIONS
         * @see Lyris API Docs Block 2.0
         *  Omitted 2.2, 2.6 to 2.8
         */

        /**
         * Query Attribute
         * @param string $mlid
         * @param int $attributeid
         * @param string $listapipass
         * @throws \Exception
         * @return array
         */
        public function demographicQuery($mlid, $attributeid, $listapipass = null)
        {

            //if api pass is set in constructor assume lyris global api pass is enabled and use for all requests
            $this->setApiPassword($listapipass);

            if (empty($mlid)) {
                throw new \Exception('MLID required');
            }

            //build data for query
            $querydata = array(
                'type'     => 'demographic',
                'activity' => 'query',
                'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="id">' . $attributeid . '</DATA>
                            <MLID>' . $mlid . '</MLID>
						</DATASET>'
            );

            //submit data
            $response = $this->submit($querydata);
            #echo $response;exit;
            //convert xml to array
            $responseobj = new \SimpleXMLElement($response, LIBXML_NOCDATA);


            //check for errors
            if ((string) $responseobj->TYPE !== 'success') {
                throw new \Exception('Query demographic failed with message: ' . (string) $responseobj->DATA);
            }

            $returnarray = array();
            foreach($responseobj->RECORD->children() as $child){
                if ( (string)$child['type'] == "option" ){
                    $returnarray['option'][] = (string)$child;
                }else{
                    $returnarray[(string)$child['type']] = (string)$child;
                }
            }

            return $returnarray;

        }

        /**
         * Query All Attribute
         * @param string $mlid
         * @param string $listapipass
         * @throws \Exception
         * @return array
         */
            public function demographicQueryAll($mlid, $listapipass = null)
                {

                    //if api pass is set in constructor assume lyris global api pass is enabled and use for all requests
                    $this->setApiPassword($listapipass);
					
                    if (empty($mlid)) {
						throw new \Exception('MLID required');
                    }
					
                    //build data for query
                    $querydata = array(
                        'type'     => 'demographic',
                        'activity' => 'query-all',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <MLID>' . $mlid . '</MLID>
						</DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
					#echo $response;exit;
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Query demographic enabled list failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $demographics = array();
                    $i = 0;
                    foreach ($responseobj->RECORD as $value) {

                        $demographics[$i] = array();

                        foreach ($value->DATA as $k => $v) {

                            $demographics[$i][(string) $v['type']] = (string) $v;

                        }
                        $i++;
                    }
					
					$returnarray['records'] = array();
					foreach($demographics as $row => $demographic){
						$returnarray['records'][  $demographic['name']  ] = $demographic;
					}
					
                    return $returnarray;
					
				}


        /**
         * Update Attribute
         * @param string $mlid
         * @param int $attributeid
         * @param string $name
         * @param string $type
         * @param string|array $value
         * @param string $state (enabled or disabled)
         * @param string $listapipass
         * @throws \Exception
         * @return array
         */
        public function demographicUpdate($mlid, $attributeid, $name, $type, $value, $state = "enabled", $listapipass = null)
        {

            //if api pass is set in constructor assume lyris global api pass is enabled and use for all requests
            $this->setApiPassword($listapipass);

            if (empty($mlid)) {
                throw new \Exception('MLID required');
            }
            if (empty($attributeid)) {
                throw new \Exception('attributeid required');
            }
            if (empty($name)) {
                throw new \Exception('name required');
            }
            if (empty($type)) {
                throw new \Exception('type required');
            }
            if (empty($value)) {
                throw new \Exception('value required');
            }

            if ( is_array($value) ){
                $options = '';
                foreach($value as $val){
                    $options .= '
                            <DATA type="option">'.$val.'</DATA>';
                }
            }else{
                $options = '<DATA type="option">'.$value.'</DATA>';
            }

            //build data for query
            $querydata = array(
                'type'     => 'demographic',
                'activity' => 'update',
                'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="id">' . $attributeid . '</DATA>
                            <DATA type="'.$type.'">' . $name . '</DATA>
                            '.$options.'
                            <DATA type="state">' . $state . '</DATA>
                            <MLID>' . $mlid . '</MLID>
						</DATASET>'
            );

            //submit data
            $response = $this->submit($querydata);
            #echo $response;exit;
            //convert xml to array
            $responseobj = new \SimpleXMLElement($response, LIBXML_NOCDATA);


            //check for errors
            if ((string) $responseobj->TYPE !== 'success') {
                throw new \Exception('Update demographic failed with message: ' . (string) $responseobj->DATA);
            }


            return $responseobj;

        }

        /**
         * Demographic Add
         * @param string $mlid
         * @param string $listapipass
         * @throws \Exception
         * @return array
         */
            public function demographicAdd($mlid, $name, $attributetype, $attributeoptions = null , $attributeenabled = true, $listapipass = null)
                {

                    //if api pass is set in constructor assume lyris global api pass is enabled and use for all requests
                    $this->setApiPassword($listapipass);
					
                    if (empty($mlid)) {
						throw new \Exception('MLID required');
                    }
					
					$dataset = '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="'. $attributetype .'">'.$name.'</DATA>';
					
					//build options list
					if ( is_array($attributeoptions) ){
						foreach($attributeoptions as $value){
							$dataset.= '
                            <DATA type="option">' . $value . '</DATA>';
						}
					}
					
					//check if enabled
					if ( $attributeenabled ){
						$dataset.= '
                            <DATA type="state">enabled</DATA>';
					}
					
					$dataset.= '
                       </DATASET>';
					
                    //build data for query
                    $querydata = array(
                        'type'     => 'demographic',
                        'activity' => 'add',
                        'input'    => $dataset
                    );

                    //submit data
                    $response = $this->submit($querydata);
					#echo $response;exit;
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Add demographic list failed with message: ' . (string) $responseobj->DATA);
                    }
					
                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['id'] = (string) $responseobj->DATA;
					
                    return $returnarray;
					
				}

        /**
         * Query Enabled Attribute
         * @param string $mlid
         * @param string $listapipass
         * @throws \Exception
         * @return array
         */
            public function demographicQueryEnabled($mlid, $listapipass = null)
                {

                    //if api pass is set in constructor assume lyris global api pass is enabled and use for all requests
                    $this->setApiPassword($listapipass);
					
                    if (empty($mlid)) {
						throw new \Exception('MLID required');
                    }
					
                    //build data for query
                    $querydata = array(
                        'type'     => 'demographic',
                        'activity' => 'query-enabled',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <MLID>' . $mlid . '</MLID>
						</DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
					#echo $response;exit;
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Query demographic enabled list failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $demographics = array();
                    $i = 0;
                    foreach ($responseobj->RECORD as $value) {

                        $demographics[$i] = array();

                        foreach ($value->DATA as $k => $v) {

                            $demographics[$i][(string) $v['type']] = (string) $v;

                        }
                        $i++;
                    }
					
					$returnarray['records'] = array();
					foreach($demographics as $row => $demographic){
						$returnarray['records'][  $demographic['name']  ] = $demographic;
					}                    return $returnarray;
					
				}
        /**
         * LIST FUNCTIONS
         * @see Lyris API Docs Block 2.0
         *  Omitted 2.2, 2.6 to 2.8
         */

        /**
         * Add Mailing List
         * @see Block 2.1
         * @param string $name
         * @param array  $attributes
         * @param string $listapipass
         * @throws \Exception
         * @return array
         */
            public function listAdd($name, array $attributes = array(), $listapipass = null)
                {

                    //if api pass is set in constructor assume lyris global api pass is enabled and use for all requests
                    $this->setApiPassword($listapipass);

                    if (empty($name)) {
						throw new \Exception('List name required');
                    }

                    //loop through attributes and add to query
                    $attributesstring = '';
                    if (!empty($attributes)) {
                        foreach ($attributes as $k => $v) {
                            $attributesstring .= PHP_EOL . '<DATA type="extra" id="' . strtoupper(trim($k)) . '">' . trim($v) . '</DATA>' . PHP_EOL;
                        }
                    }

                    //build data for query
                    $querydata = array(
                        'type'     => 'list',
                        'activity' => 'add',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="name">' . $name . '</DATA>' . $attributesstring . '</DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Adding of list failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    foreach ($responseobj->DATA as $value) {
                        $returnarray[(string) $value['type']] = (string) $value;
                    }

                    return $returnarray;
                }

        /**
         * Delete Mailing List
         * @see Block 2.3
         * @param string $mlid
         * @param        $listapipass
         * @throws \Exception
         * @return array
         */
            public function listDelete($mlid, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build query array
                    $querydata = array(
                        'type'     => 'list',
                        'activity' => 'delete',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <MLID>' . $mlid . '</MLID>
                             </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Removing of list failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['message'] = (string) $responseobj->DATA;

                    return $returnarray;
                }

        /**
         * Edit Mailing List
         * @see Block 2.4
         * @param string $mlid
         * @param string $name
         * @param string $from_name
         * @param string $from_email
         * @param string $listapipass
         * @throws \Exception
         * @return array
         */
            public function listEdit($mlid, $name, $from_name, $from_email, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'list',
                        'activity' => 'edit',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="name">' . $name . '</DATA>
                            <DATA type="extra" id="FROM_NAME">' . $from_name . '</DATA>
                            <DATA type="extra" id="FROM_EMAIL">' . $from_email . '</DATA>
                            </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Editing of list failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['message'] = (string) $responseobj->DATA;

                    return $returnarray;
                }

        /**
         * Query for all lists and relevent data
         * @see Block 2.5
         * @param string $listapipass
         * @throws \Exception
         * @return array
         */
            public function listQuery($listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build query array
                    $querydata = array(
                        'type'     => 'list',
                        'activity' => 'query-listdata',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
					
                    //create new SimpleXMLElement Instance
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('List Query failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $i = 0;
                    foreach ($responseobj->RECORD as $value) {

                        $returnarray[$i] = array();

                        //add list id as array value
                        $returnarray[$i]['mlid'] = (string) $value->DATA['id'];

                        foreach ($value->DATA as $k => $v) {

                            $returnarray[$i][(string) $v['type']] = (string) $v;

                        }
                        $i++;
                    }

                    return $returnarray;
                }

        /**
         * MEMBER FUNCTIONS
         * @see Lyris API Docs Block 4.0
         */

        /**
         * Add member to list
         * @see Block 4.1
         * @param int     $mlid
         * @param string  $email
         * @param array   $demographics
         * @param array   $attributes
         * @param string  $listapipass
         * @throws \Exception
         * @return array
         */
            public function memberAdd($mlid, $email, array $demographics = null, array $attributes = null, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //loop through attributes and add to query
                    $attributesstring = '';
                    if (!empty($attributes)) {
                        foreach ($attributes as $k => $v) {
                            $attributesstring .= PHP_EOL . '<DATA type="extra" id="' . strtoupper(trim($k)) . '">' . trim($v) . '</DATA>' . PHP_EOL;
                        }
                    }

                    //loop through demographics and add to query
                    $demographicsstring = '';
                    if (!empty($demographics)) {
                        foreach ($demographics as $k => $v) {
                            $demographicsstring .= PHP_EOL . '<DATA type="demographic" id="' . trim($k) . '">' . trim($v) . '</DATA>' . PHP_EOL;
                        }
                    }

                    //build data for query
                    $querydata = array(
                        'type'     => 'record',
                        'activity' => 'add',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="email">' . $email . '</DATA>' . $attributesstring . $demographicsstring . '</DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);
                    //check for success
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Adding Member failed with message: ' . (string) $responseobj->DATA);
                    }
                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['memberid'] = (string) $responseobj->DATA;

                    return $returnarray;
                }

        /**
         * Download Members of Mailing list
         * @see Block 4.2
         * @param type   $mlid
         * @param type   $email
         * @param type   $type
         * @param string $listapipass
         * @throws \Exception
         * @return array
         */
            public function memberDownload($mlid, $email, $type, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'record',
                        'activity' => 'download',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="extra" id="email_notify">' . $email . '</DATA>
                            <DATA type="extra" id="type">' . $type . '</DATA>
                            </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Member download failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;

                    return $returnarray;
                }


        /**
         * Upload Members to Mailing list
         * @see Block 4.6
         * @param id    $mlid
         * @param string   $emailNotify
         * @param string   $file
         * @param string   $type       active | proof | unsubscribed | bounced | trashed | globalunsubscribe
         * @param string   $append
         * @param string   $applyTriggers
         * @param string   $clearTriggerHistory
         * @param string   $deleteBlank
         * @param string   $update
         * @param string   $untrash
         * @param string   $validate
         * @param string   $delimiter
         * @param string $listapipass
         * @throws \Exception
         * @return array
         */
        public function memberUpload($mlid, $emailNotify, $file, $type, $append = NULL, $applyTriggers = NULL, $clearTriggerHistory = NULL, $deleteBlank = NULL, $update = NULL, $untrash = NULL, $validate = NULL, $delimiter = ",", $listapipass = null)
        {

            //check api password
            $this->setApiPassword($listapipass);

            //build data for query
            $querydata = array(
                'type'     => 'record',
                'activity' => 'upload',
                'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="email" id="email">' . $emailNotify . '</DATA>
                            <DATA type="extra" id="file">' . $file . '</DATA>
                            <DATA type="extra" id="type">' . $type . '</DATA>
                            <DATA type="extra" id="delimiter">' . $delimiter . '</DATA>'
            );

            //optional params
            if ( $append ){
                $querydata['input'] .= '
                            <DATA type="extra" id="append">' . $append . '</DATA>';
            }
            if ( $applyTriggers ){
                $querydata['input'] .= '
                            <DATA type="extra" id="trigger">' . $applyTriggers . '</DATA>';
            }
            if ( $clearTriggerHistory ){
                $querydata['input'] .= '
                            <DATA type="extra" id="clear_trigger_history">' . $clearTriggerHistory . '</DATA>';
            }
            if ( $deleteBlank ){
                $querydata['input'] .= '
                            <DATA type="extra" id="delete_blank">' . $deleteBlank . '</DATA>';
            }
            if ( $update ){
                $querydata['input'] .= '
                            <DATA type="extra" id="update">' . $update . '</DATA>';
            }
            if ( $untrash ){
                $querydata['input'] .= '
                            <DATA type="extra" id="untrash">' . $untrash . '</DATA>';
            }
            if ( $validate ){
                $querydata['input'] .= '
                            <DATA type="extra" id="validate">' . $validate . '</DATA>';
            }

            //end dataset
            $querydata['input'] .= '
                            </DATASET> ';

            //submit data
            $response = $this->submit($querydata);
            //convert xml to array
            $responseobj = new \SimpleXMLElement($response);

            //check for errors
            if ((string) $responseobj->TYPE !== 'success') {
                throw new \Exception('Member upload failed with message: ' . (string) $responseobj->DATA);
            }

            //create blank array to bind items to
            $returnarray = array();
            //clean up result and return array
            $returnarray['status'] = (string) $responseobj->TYPE;

            return $returnarray;
        }


        /**
         * Query Members
         * @see Block 4.3
         * @param numeric $mlid
         * @param string  $email
         * @param string  $listapipass
         * @throws \Exception
         * @return array
         */
            public function memberQuery($mlid, $email, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'record',
                        'activity' => 'query-data',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="email">' . $email . '</DATA>
                            </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);

                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Member Query failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;

                    if ($returnarray['status'] == 'error') {
                        $returnarray['message'] = (string) $responseobj->DATA;
                    }

                    foreach ($responseobj->RECORD->DATA as $value) {
	                    /*
	                     * Determine if there are multiple values for one ID
	                     * and create array if necessary.
	                     */
                        if ( isset($returnarray[(string) $value['id']]) ) {

	                        if (!is_array($returnarray[(string)$value['id']])) {
		                        $originalvalue = $returnarray[(string)$value['id']];
		                        $returnarray[(string)$value['id']] = array();
		                        $returnarray[(string)$value['id']][] = $originalvalue;
	                        }
                            $returnarray[(string)$value['id']][] = (string)$value;
                        }else {
                            $returnarray[(string)$value['id']] = (string)$value;
                        }
                    }

                    return $returnarray;
                }

        /**
         * Query list for member data
         * @see Block 4.4
         * @param      $mlid
         * @param      $type
         * @param null $listapipass
         * @throws \Exception
         * @return array
         */
            public function memberQueryList($mlid, $type, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'record',
                        'activity' => 'query-listdata',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="extra" id="type">' . $type . '</DATA>
                            </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Query member list failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;

                    if ($returnarray['status'] == 'error') {
                        $returnarray['message'] = (string) $responseobj->DATA;
                    }

                    foreach ($responseobj->RECORD->DATA as $value) {
                        if ((string) $value['type'] == 'email') {
                            $returnarray[(string) $value['type']] = (string) $value;
                        } else {
                            $returnarray[(string) $value['id']] = (string) $value;
                        }
                    }

                    return $returnarray;

                }

        /**
         * Return statistical data for a single contact
         * @see Block 4.5
         * @param        $mlid
         * @param string $email
         * @param string $start_date (YYYY-MM-DD, optional)
         * @param string $end_date (YYYY-MM-DD, optional)
         * @param null   $listapipass
         * @throws \Exception
         * @return array
         */

         /*
          * @author Ryan Field
          * @since June 14, 2015, 5:13:32 PM
          * @link https://github.com/kokuou
          * @copyright 2015
          */
            public function memberQueryStats($mlid, $email, $start_date = null, $end_date = null, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //check if $start_date and $end_date are null
                    $start_date = $start_date === null ? '' : '<DATA type="extra" id="start_date">'.$start_date.'</DATA>';
                    $end_date = $end_date === null ? '' : '<DATA type="extra" id="end_date">'.$end_date.'</DATA>';

                    //build data for query
                    $querydata = array(
                        'type'     => 'record',
                        'activity' => 'query-stats',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="email">' . $email . '</DATA>
                            '.$start_date.$end_date.'
                            </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Contact Management - Query-Stats failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();

                    //create array to add records to
                    $record_array = array();

                    //iterate through each <RECORD>
                    foreach($responseobj->RECORD as $record) {
                        //iterate through each DATA child node and add information to array
                        $temp_record = array();
                        foreach($record->DATA as $data) {
                            $attr = $data->attributes();
                            $type = (string)$attr['type'];

                            $item = $type;
                            //if there is no id set, ignore
                            if (isset($attr['id'])) {
                                $item .= '-'.(string)$attr['id'];
                            }
                            $temp_record[$item] = (string) $data;
                        }
                        //add $temp_record to $record_array to return
                        $record_array[] = $temp_record;
                  }

                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['records'] = (array) $record_array;

                    return $returnarray;
                }

        /**
         * Edit List Member(Subscriber)
         * @see Block 4.6
         * @param string $mlid
         * @param string $email
         * @param array  $attributes
         * @param array  $demographics
         * @param        $listapipass
         * @throws \Exception
         * @return array
         */
            public function memberEdit($mlid, $email, array $attributes, array $demographics = null, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //loop through attributes and add to query
                    $attributesstring = '';
                    if (!empty($attributes)) {
                        foreach ($attributes as $k => $v) {
                            $attributesstring .= PHP_EOL . '<DATA type="extra" id="' . trim($k) . '">' . trim($v) . '</DATA>' . PHP_EOL;
                        }
                    }

                    //loop through demographics and add to query
                    $demographicsstring = '';
                    if (!empty($demographics)) {
                        foreach ($demographics as $k => $v) {

	                        if ( is_array($v) ) {
		                        $v = implode('||', $v);
	                        }

	                        $demographicsstring .= PHP_EOL . '<DATA type="demographic" id="' . trim($k) . '">' . trim($v) . '</DATA>' . PHP_EOL;
                        }
                    }

                    //build data for query
                    $querydata = array(
                        'type'     => 'record',
                        'activity' => 'update',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="email">' . $email . '</DATA>' . $attributesstring . $demographicsstring . '</DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Editing of member failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['memberid'] = (string) $responseobj->DATA;

                    return $returnarray;
                }

        /**
         * Message Management
         * @see Lyris API Docs Block 5.0
         */

        /**
         * Add Message
         * @see Block 5.1
         * @param string   $mlid
         * @param string   $fromEmail
         * @param string   $fromName
         * @param string   $subject
         * @param string   $messageFormat
         * @param string   $messageText
         * @param string   $messageHTML
         * @param          $listapipass
         * @throws \Exception
         * @return array
         */
            public function messageAdd($mlid, $fromEmail, $fromName, $subject, $messageFormat, $messageText, $messageHTML, $attachmentURL = null, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //check if format is html if so add messagehtml to querydata
                    if (strtoupper($messageFormat) == 'HTML') {
                        $html = '<DATA type="message-html">' . htmlentities($messageHTML, NULL, "UTF-8") . '</DATA>';
                    } else {
                        $html = '';
                    }

                    // Check if this messageAdd is an attachment
                    if ( $attachmentURL ){
                        $html .= '<DATA type="attachment">'. $attachmentURL .'</DATA>';
                        $html .= '<DATA type="category">newsletter</DATA>';;
                        $html .= '<DATA type="clickthru">on</DATA>';
                        $html .= '<DATA type="clickthru-text">on</DATA>';
                    }

                    //build data for query
                    $querydata = array(
                        'type'     => 'message',
                        'activity' => 'add',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="subject">' . $subject . '</DATA>
                            <DATA type="from-email">' . $fromEmail . '</DATA>
                            <DATA type="from-name">' . $fromName . '</DATA>
                            <DATA type="message-format">' . $messageFormat . '</DATA>
                            <DATA type="message-text">' . $messageText . '</DATA>' . $html . '</DATASET>
                            <DATA type="charset">UTF-8</DATA>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check if query was successful if not throw exception
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Adding Message Failed with message: ' . $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['messageid'] = (string) $responseobj->DATA;

                    return $returnarray;
                }

        /**
         * Copy message
         * @see Block 5.2
         * @param      $mlid
         * @param      $mid
         * @param null $listapipass
         * @throws \Exception
         * @return array
         */
            public function messageCopy($mlid, $mid, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'message',
                        'activity' => 'copy',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MID>' . $mid . '</MID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Coping of message failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['messageid'] = (string) $responseobj->DATA;

                    return $returnarray;
                }

        /**
         * Send quick proof of message to emails
         * @see Block 5.3
         * @param        $mlid
         * @param        $mid
         * @param        $emails
         * @param string $contentanalyzer
         * @param string $inboxsnap
         * @param string $blmon
         * @param string $multi
         * @param null   $listapipass
         * @throws \Exception
         * @return array
         */
            public function messageQuickTest($mlid, $mid, $emails, $contentanalyzer = 'off', $inboxsnap = 'off', $blmon = 'off', $multi = '1', $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //explode emails array into csv format
                    if (is_array($emails)) {
                        $emails = implode(', ', $emails);
                    }

                    //build data for query
                    $querydata = array(
                        'type'     => 'message',
                        'activity' => 'message-quicktest',
                        'input'    => '<DATASET>
                    <SITE_ID>' . $this->siteid . '</SITE_ID>
                    <MID>' . $mid . '</MID>
                    <MLID>' . $mlid . '</MLID>
                    <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                    <DATA type="extra" id="emails">' . $emails . '</DATA>
                    <DATA type="extra" id="content_analyzer">' . $contentanalyzer . '</DATA>
                    <DATA type="extra" id="inbox_snapshot">' . $inboxsnap . '</DATA>
                    <DATA type="extra" id="back_list_monitor">' . $blmon . '</DATA>
                    <DATA type="extra" id="multi">' . $multi . '</DATA>
                    </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Message quick test failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['message'] = (string) $responseobj->DATA;
                    $returnarray['htmlmid'] = (string) $responseobj->HTML_MID;
                    $returnarray['textmid'] = (string) $responseobj->TEXT_MLID;
                    $returnarray['contentanalyizer'] = (string) $responseobj->CONTENT_ANALYZER;
                    $returnarray['inboxsnap'] = (string) $responseobj->INBOX_SNAPSHOT;

                    return $returnarray;
                }

        /**
         * Proof a created message
         * @see Block 5.4
         * @param      $mlid
         * @param      $mid
         * @param      $text
         * @param null $listapipass
         * @throws \Exception
         * @return array
         */
            public function messageProof($mlid, $mid, $text, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'message',
                        'activity' => 'proof',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MID>' . $mid . '</MID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="text">' . $text . '</DATA>
                            </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'Success') {
                        throw new \Exception('Message proofing failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['message'] = (string) $responseobj->DATA;
                    foreach ($responseobj->EMAIL as $email) {
                        $returnarray['email'][] = (string) $email;
                    }

                    return $returnarray;
                }

        /**
         * Gathers all stats of single message
         * @see Block 5.5
         * @param      $mlid
         * @param      $mid
         * @param null $listapipass
         * @return array
         * @throws \Exception
         */
            public function messageQueryData($mlid, $mid, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'message',
                        'activity' => 'query-data',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MLID>' . $mlid . '</MLID>
                            <MID>' . $mid . '</MID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Message data query failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;

                    foreach ($responseobj->RECORD->DATA as $value) {
                        $returnarray[(string) $value['type']] = (string) $value;
                    }

                    return $returnarray;
                }

        /**
         * Get all messages in a list
         * @see Block 5.6
         * @param      $mlid
         * @param null $startdate
         * @param null $enddate
         * @param null $listapipass
         * @throws \Exception
         * @return array
         */

        /*
         * @author Henry Paradiz
         * @since Jan 7, 2013, 10:04:32 AM
         * @link http://akuj.in
         * @copyright 2013
         */
            public function messageQueryListData($mlid, $startdate = null, $enddate = null, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'message',
                        'activity' => 'query-listdata',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Message list query failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $output = array();

                    //parse through return data and bind to output array
                     foreach($responseobj->children() as $Record)
                     {
                       $temp = array();
                       foreach($Record->children() as $Data)
                       {
                         $Attributes = $Data->attributes();

                         $Type = (string) $Attributes['type'][0];

                         $temp[$Type] = (string) $Data;
                       }

                       if(count($temp))
                       {
                         $output[] = $temp;
                       }
                     }

                     return $output;
                }

        /**
         * Query Messages Stats
         * @see Block 5.10
         * @param      $mid
         * @param      $mlid
         * @param      $action
         * @param null $params
         * @param null $listapipass
         * @throws \Exception
         * @return array
         */

        /*
         * @author Henry Paradiz
         * @since Jan 7, 2013, 10:04:32 AM
         * @link http://akuj.in
         * @copyright 2013
         */
            public function messageQueryStats($mid, $mlid, $action = null, $params = null, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    if ($action == 'clicked-details-background') {
                        $email = '<DATA type="extra" id="email">' . $params['email'] . '</DATA>';
                    } else {
                        $email = '';
                    }

                    //build data for query
                    $querydata = array(
                        'type'     => 'message',
                        'activity' => 'query-stats',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MID>' . $mid . '</MID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="' . strtoupper($action) . '"></DATA>' . $email . '</DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Query message stats failed with message: ' . (string) $responseobj->DATA);
                    }

                    $temp = array();
                    foreach($responseobj->RECORD->children() as $Data)
                    {
                      $Attributes = $Data->attributes();

                       $Type = (string) $Attributes['type'][0];

                       $temp[$Type] = (string) $Data;
                    }

                    return $temp;
                }

        /**
         * Schedule Message
         * @see Block 5.12
         * @param       $mlid
         * @param       $mid
         * @param       $action
         * @param       $timestamp
         * @param array $attributes
         * @param null  $listapipass
         * @throws \Exception
         * @return array
         */
            public function messageSchedule($mlid, $mid, $action, $timestamp = null, array $attributes = null, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //check if action is schedule if so add delivery dates to query
                    $timestampstring = '';
                    //if action is schedule and timestamp is after current date add them to the query if not leave null for immediate send
                    if ($action == 'schedule' && strtotime($timestamp) > time()) {
                        $timestampstring .= '<DATA type="delivery-year">' . date('Y', strtotime($timestamp)) . '</DATA>';
                        $timestampstring .= '<DATA type="delivery-month">' . date('n', strtotime($timestamp)) . '</DATA>';
                        $timestampstring .= '<DATA type="delivery-day">' . date('j', strtotime($timestamp)) . '</DATA>';
                        $timestampstring .= '<DATA type="delivery-hour">' . date('G', strtotime($timestamp)) . '</DATA>';
                    }

                    //loop through attributes and add to query
                    $attributesstring = '';
                    if (!empty($attributes)) {
                        foreach ($attributes as $k => $v) {
                            $attributesstring .= PHP_EOL . '<DATA type="' . strtoupper(trim($k)) . '">' . trim($v) . '</DATA>' . PHP_EOL;
                        }
                    }

                    //build data for query
                    $querydata = array(
                        'type'     => 'message',
                        'activity' => 'schedule',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MID>' . $mid . '</MID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            <DATA type="action">' . $action . '</DATA>' . $timestampstring . $attributesstring . '</DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check if response was successful if not throw exception
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Scheduling of message failed with message: ' . $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['messageid'] = (string) $responseobj->DATA;

                    return $returnarray;
                }

        /**
         * Edit An already created message
         * @see Block 5.14
         * @param      $mlid
         * @param      $mid
         * @param      $fromEmail
         * @param      $fromName
         * @param      $subject
         * @param      $messageFormat
         * @param      $messageText
         * @param      $messageHTML
         * @param null $listapipass
         * @throws \Exception
         * @return array
         */
            public function messageEdit($mlid, $mid, $fromEmail, $fromName, $subject, $messageFormat, $messageText, $messageHTML, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    // Update $fromEmail
                    $fromEmailHTML = '';
                    if ( !empty($fromEmail) ){
                        $fromEmailHTML = '<DATA type="from-email">' . $fromEmail . '</DATA>';
                    }

                    // Update $fromName
                    $fromNameHTML = '';
                    if ( !empty($fromName) ){
                        $fromNameHTML = '<DATA type="from-name">' . $fromName . '</DATA>';
                    }

                    // Update subject
                    $subjectHTML = '';
                    if ( !empty($subject) ){
                        $subjectHTML = '<DATA type="subject">' . $subject . '</DATA>';
                    }

                    // Update $messageFormat
                    $messageFormatHTML = '';
                    if ( !empty($messageFormat) ){
                        $messageFormatHTML = '<DATA type="message-format">' . $messageFormat . '</DATA>';
                    }

                    // Update $messageText
                    $messageTextHTML = '';
                    if ( !empty($messageText) ){
                        $messageTextHTML = '<DATA type="message-text">' . $messageText . '</DATA>';
                    }

                    // Update $messageHTML
                    $messageHTMLHTML = '';
                    if ( !empty($messageHTML) ){
                        $messageHTMLHTML = '<DATA type="message-html">' . htmlspecialchars($messageHTML, NULL, "UTF-8") . '</DATA>';
                    }

                    //build data for query
                    $querydata = array(
                        'type'     => 'message',
                        'activity' => 'update',
                        'input'    => '<DATASET>
                            <SITE_ID>' . $this->siteid . '</SITE_ID>
                            <MID>' . $mid . '</MID>
                            <MLID>' . $mlid . '</MLID>
                            <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                            '.$fromEmailHTML.'
                            '.$fromNameHTML.'
                            '.$subjectHTML.'
                            '.$messageFormatHTML.'
                            '.$messageTextHTML.'
                            '.$messageHTMLHTML.'
                            </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Editing of message failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();
                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['messageid'] = (string) $responseobj->DATA;

                    return $returnarray;
                }

        /**
         * SEGMEMT FUNCTIONS
         * @see Lyris API Docs Block 6.0
         */

        /**
         * Regenerate segment
         * @see Block 6.3
         * @param        $mlid
         * @param string $segment_id
         * @param string $email
         * @param null   $listapipass
         * @throws \Exception
         * @return array
         */

         /*
          * @author Ryan Field
          * @since June 14, 2015, 5:13:32 PM
          * @link https://github.com/kokuou
          * @copyright 2015
          */

            public function filterGenerate($mlid, $segment_id, $email, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'filter',
                        'activity' => 'generate',
                        'input'    => '<DATASET>
                        <SITE_ID>' . $this->siteid . '</SITE_ID>
                        <MLID>' . $mlid . '</MLID>
                        <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                        <DATA type="id">' . $segment_id . '</DATA>
                        <DATA type="extra" id="email">' . $email . '</DATA>
                        </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);


                    //check for errors
          $response_msg = (string) $responseobj->DATA;
                    if (strpos($response_msg, 'Your segments generations has started.') === false) {
                        throw new \Exception('Segment Management - Generate failed with message: ' . (string) $responseobj->DATA);
                    }


                    //create blank array to bind items to
                    $returnarray = array();

                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->DATA;

                    return $returnarray;
                }


            public function filterAdd($mlid, $name, $demographicArray, $extraArray = null, $listapipass = null){

                //check api password
                $this->setApiPassword($listapipass);

                //build data for DEMOGRAPHIC
                $demographicdata = "";
                foreach($demographicArray as $demographic){
                    $demographicdata .= '<DATA type="demographic"';
                    foreach($demographic as $field=>$value){
                        if ( $field == "value" || $field == "type" ) continue;
                        $demographicdata .= ' '.$field.'="'.$value.'"';
                    }
                    $demographicdata .= '>'.$demographic['value'].'</DATA>';
                }

                //build data for EXTRA
                $extradata = "";
                if ( !empty($extraArray) ) {
                    foreach ($extraArray as $extra) {
                        $extradata .= '<DATA type="extra"';
                        foreach ($extra as $field => $value) {
                            if ($field == "value" || $field == "type") continue;
                            $extradata .= ' ' . $field . '="' . $value . '"';
                        }
                        $extradata .= '>' . $extra['value'] . '</DATA>';
                    }
                }


                //build data for query
                $querydata = array(
                    'type'     => 'filter',
                    'activity' => 'add',
                    'input'    => '<DATASET>
                        <SITE_ID>' . $this->siteid . '</SITE_ID>
                        <MLID>' . $mlid . '</MLID>
                        <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                        <DATA type="name">' . $name . '</DATA>
                        '.$demographicdata.'
                        '.$extradata.'
                        </DATASET>'
                );

                //submit data
                $response = $this->submit($querydata);

                //convert xml to array
                $responseobj = new \SimpleXMLElement($response);

                //check for errors
                if ((string) $responseobj->TYPE !== 'Success') {
                    throw new \Exception('Segment Management - Update failed with message: ' . (string) $responseobj->DATA);
                }

                //create blank array to bind items to
                $returnarray = array();
                $returnarray['status'] = (string) $responseobj->TYPE;
                $returnarray['id'] = (string) $responseobj->DATA;

                return $returnarray;
            }

            public function filterUpdate($mlid, $name, $demographicArray, $extraArray, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for DEMOGRAPHIC

                    $demographicdata = "";
                    foreach($demographicArray as $demographic){
                        $demographicdata .= '<DATA type="demographic"';
                        foreach($demographic as $field=>$value){
                            if ( $field == "value" || $field == "type" ) continue;
                            $demographicdata .= ' '.$field.'="'.$value.'"';
                        }
                        $demographicdata .= '>'.$demographic['value'].'</DATA>';
                    }

                    //build data for EXTRA
                    $extradata = "";
                    foreach($extraArray as $extra){
                        $extradata .= '<DATA type="extra"';
                        foreach($extra as $field=>$value){
                            if ( $field == "value" || $field == "type" ) continue;
                            $extradata .= ' '.$field.'="'.$value.'"';
                        }
                        $extradata .= '>'.$extra['value'].'</DATA>';
                    }

                    //build data for query
                    $querydata = array(
                        'type'     => 'filter',
                        'activity' => 'update',
                        'input'    => '<DATASET>
                        <SITE_ID>' . $this->siteid . '</SITE_ID>
                        <MLID>' . $mlid . '</MLID>
                        <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                        <DATA type="name">' . $name . '</DATA>
                        '.$demographicdata.'
                        '.$extradata.'
                        </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);

                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'Success') {
                        throw new \Exception('Segment Management - Update failed with message: ' . (string) $responseobj->DATA);
                    }


                    //create blank array to bind items to
                    $returnarray = array();
                    $returnarray['status'] = (string) $responseobj->TYPE;

                    return $returnarray;
                }

        /**
         * Retrieve all records for a single segment
         * @see Block 6.4
         * @param        $mlid
         * @param        $segment_id
         * @param null   $listapipass
         * @throws \Exception
         * @return array
         */

         /*
          * @author Ryan Field
          * @since June 14, 2015, 5:13:32 PM
          * @link https://github.com/kokuou
          * @copyright 2015
          */

            public function filterQueryData($mlid, $segment_id, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'filter',
                        'activity' => 'query-data',
                        'input'    => '<DATASET>
                        <SITE_ID>' . $this->siteid . '</SITE_ID>
                        <MLID>' . $mlid . '</MLID>
                        <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                        <DATA type="id">' . $segment_id . '</DATA>
                        </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Segment Management - Query-ListData failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();

                    //create array to add records to
                    $record_array = array();

                    //iterate through each <RECORD>
                    foreach($responseobj->RECORD as $record) {

                        //iterate through each DATA child node and add information to array
                        $temp_record = array();

                        foreach($record->DATA as $data) {
                            $attr = $data->attributes();
                            $type = (string)$attr['type'];

                            $item = $type;
                            //if there is no id set, ignore
                            if (isset($attr['id'])) {
                                $item .= '-'.(string)$attr['id'];
                            }

                            $temp_record[$item] = (string) $data;
                        }
                        //add $temp_record to $record_array
                        $record_array[] = $temp_record;

                  }

                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['records'] = (array) $record_array;

                    return $returnarray;
              }

         /*
          * @author Ryan Field
          * @since June 14, 2015, 5:13:32 PM
          * @link https://github.com/kokuou
          * @copyright 2015
          */

            public function filterQueryListData($mlid, $sortorder = "desc", $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'filter',
                        'activity' => 'query-listdata',
                        'input'    => '<DATASET>
                        <SITE_ID>' . $this->siteid . '</SITE_ID>
                        <MLID>' . $mlid . '</MLID>
                        <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                        <DATA type="extra" id="sortorder">' . $sortorder . '</DATA>
                        </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);

                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for no records
                    if ( (string) $responseobj->TYPE == 'norecords' ){
                        return array('status'=>'success', 'records'=>array());
                    }
                    
                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Segment Management - Query-ListData failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();

                    //create array to add records to
                    $record_array = array();

                    //iterate through each <RECORD>
                    foreach($responseobj->RECORD as $record) {

                        //iterate through each DATA child node and add information to array
                        $temp_record = array();
                        $temp_record['demographic'] = array();

                        foreach($record->DATA as $data) {

                            $attr = $data->attributes();
                            $type = (string)$attr['type'];

                            if ( $type == 'demographic' ){
                                $tmparr = array();
                                foreach($attr as $name => $value){
                                    $tmparr[(string)$name] = (string)$value;
                                }
                                $tmparr['value'] = (string)$data;
                                $temp_record['demographic'][] = $tmparr;
                                continue;
                            }

                            if ( $type == 'extra' ){
                                $tmparr = array();
                                foreach($attr as $name => $value){
                                    $tmparr[(string)$name] = (string)$value;
                                }

                                $tmparr['value'] = (string)$data;
                                $temp_record['extra'][] = $tmparr;
                                continue;
                            }

                            $item = $type;
                            //if there is no id set, ignore
                            if (isset($attr['id'])) {
                                $item .= '-'.(string)$attr['id'];
                            }

                            $temp_record[$item] = (string) $data;
                        }
                        //add $temp_record to $record_array
                        $record_array[] = $temp_record;

                  }

                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;
                    $returnarray['records'] = (array) $record_array;

                    return $returnarray;
              }

        /*
		 * @author Ryan Field
		 * @since June 14, 2015, 5:13:32 PM
		 * @link https://github.com/kokuou
		 * @copyright 2015
		 */

        public function filterDelete($mlid, $segment_id, $listapipass = null)
        {

            //check api password
            $this->setApiPassword($listapipass);

            //build data for query
            $querydata = array(
                'type'     => 'filter',
                'activity' => 'delete',
                'input'    => '<DATASET>
                        <SITE_ID>' . $this->siteid . '</SITE_ID>
                        <MLID>' . $mlid . '</MLID>
                        <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                        <DATA type="id">' . $segment_id . '</DATA>
                        </DATASET>'
            );

            //submit data
            $response = $this->submit($querydata);
            //convert xml to array
            $responseobj = new \SimpleXMLElement($response);

            //check for errors
            if ((string) $responseobj->TYPE !== 'success') {
                throw new \Exception('Segment Management - Query-Delete failed with message: ' . (string) $responseobj->DATA);
            }

            //create blank array to bind items to
            $returnarray = array();

            //clean up result and return array
            $returnarray['status'] = (string) $responseobj->TYPE;

            return $returnarray;
        }
        /**
         * TRIGGER FUNCTIONS
         * @see Lyris API Docs Block 8.0
         */

        /**
         * Fire a trigger that has already been set up
         * @see Block 8.4
         * @param        $mlid
         * @param        $trigger_id
         * @param opt    $recipients
         * @param null   $listapipass
         * @throws \Exception
         * @return array
         */

         /*
          * @author Ryan Field
          * @since June 14, 2015, 5:13:32 PM
          * @link https://github.com/kokuou
          * @copyright 2015
          */

            public function triggersFireTrigger($mlid, $trigger_id, $recipients, $listapipass = null)
                {

                    //check api password
                    $this->setApiPassword($listapipass);

                    //build data for query
                    $querydata = array(
                        'type'     => 'triggers',
                        'activity' => 'fire-trigger',
                        'input'    => '<DATASET>
                        <SITE_ID>' . $this->siteid . '</SITE_ID>
                        <MLID>' . $mlid . '</MLID>
                        <DATA type="extra" id="password">' . $this->apipassword . '</DATA>
                        <DATA type="extra" id="trigger_id">' . $trigger_id . '</DATA>
                        <DATA type="extra" id="recipients">' . $recipients . '</DATA>
                        </DATASET>'
                    );

                    //submit data
                    $response = $this->submit($querydata);
                    //convert xml to array
                    $responseobj = new \SimpleXMLElement($response);

                    //check for errors
                    if ((string) $responseobj->TYPE !== 'success') {
                        throw new \Exception('Trigger Management - Fire-Trigger failed with message: ' . (string) $responseobj->DATA);
                    }

                    //create blank array to bind items to
                    $returnarray = array();


                    //clean up result and return array
                    $returnarray['status'] = (string) $responseobj->TYPE;

                    return $returnarray;
                }

        /**
         * Sets provided api password for use
         * @param $password
         */
            final protected function setApiPassword($password)
                {
                    if (empty($this->apipassword)) {
                        $this->apipassword = $password;
                    }
                }

        /**
         * POSTS REQUESTS TO LYRIS API
         * @param array $data
         * @return XML
         */
            final private function submit(array $data)
                {
					error_reporting(-1);ini_set('display_errors', 'On');
                    // set url var
                    $url = $this->url;
                    // open connection, or reuse existing
					if ( !$this->ch ){
						$this->ch = curl_init();
					}
                    // set the url, number of POST vars, POST data
                    curl_setopt($this->ch, CURLOPT_URL, $url);
                    curl_setopt($this->ch, CURLOPT_POST, true);
                    curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($this->ch, CURLOPT_FAILONERROR, 1);
					curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0); 
                    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($this->ch, CURLOPT_TIMEOUT, 15);
                    // execute post
                    $result = curl_exec($this->ch);
					
                    $errorno = curl_errno($this->ch);
                    $error = curl_error($this->ch);
					
					if ( $errorno !== 0 ){
                        throw new \Exception('CURL request failed (err '.$errorno.') with message: ' . (string) $error);
					}

                    return $result;
                }
				
			final function disconnect(){
                // close connection
                curl_close($this->ch);
			}
        }
